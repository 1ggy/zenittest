<?php


namespace ZenItTest\Form;

/**
 * Class Form
 * @package ZenItTest
 */
class Form
{
    /**
     * @var Field[]
     */
    private array $fields = [];

    public function addField(Field $field): self
    {
        $this->fields[] = $field;
        return $this;
    }

    public function getFields(): array
    {
        $formattedFields = [];
        foreach ($this->fields as $field) {
            $formattedFields[] = ['name' => $field->getName(), 'contents' => $field->getContents()];
        }
        return $formattedFields;
    }
}
