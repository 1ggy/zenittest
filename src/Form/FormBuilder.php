<?php


namespace ZenItTest\Form;

/**
 * Тут использован паттерн Строитель
 * Interface FormBuilder
 * @package ZenItTest\Form
 */
class FormBuilder
{
    private Form $form;

    public function addTextField(string $name, $contents): self
    {
        $textField = new TextField($name, $contents);
        $this->form->addField($textField);
        return $this;
    }

    public function createForm(): self
    {
        $this->form = new Form();
        return $this;
    }

    public function getForm(): Form
    {
        return $this->form;
    }
}
