<?php


namespace ZenItTest\Form;

use Psr\Http\Message\StreamInterface;

class TextField implements Field
{
    private string $name;
    private $contents;
    private array $headers = [];
    private string $filename;

    public function __construct(string $name, $contents)
    {
        $this->name = $name;
        $this->contents = $contents;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @inheritDoc
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    public function getFilename(): string
    {
        return $this->filename;
    }
}
