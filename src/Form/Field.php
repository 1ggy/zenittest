<?php


namespace ZenItTest\Form;

use Psr\Http\Message\StreamInterface;

/**
 * Interface Field
 * @package ZenItTest\Form
 * FIXME Тут нарушается interface segregation принцип
 */
interface Field
{
    /**
     * @return string Имя поля формы
     */
    public function getName(): string;

    /**
     * @return StreamInterface|resource|string содержимое поля
     */
    public function getContents();

    /**
     * @return array Дополнительные заголовки для форми
     */
    public function getHeaders(): array;

    /**
     * @return string имя файла
     */
    public function getFilename(): string;
}
