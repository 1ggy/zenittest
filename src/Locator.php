<?php

namespace ZenItTest;

use InvalidArgumentException;

/**
 * В локаторе применен шаблон Singleton
 * Class Locator
 * @package ZenItTest
 */
final class Locator
{
    private static ?Locator $instance = null;

    public static function getInstance(): Locator
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function __wakeup()
    {
        throw new \RuntimeException("Cannot unserialize singleton");
    }

    /**
     * @var string[][]
     */
    private array $services = [];

    /**
     * @var ServiceInterface[]
     */
    private array $instantiated = [];

    public function addService(string $class, ServiceInterface $service): void
    {
        $this->instantiated[$class] = $service;
    }

    public function addClass(string $class, array $params): void
    {
        $this->services[$class] = $params;
    }

    public function has(string $interface): bool
    {
        return isset($this->services[$interface]) || isset($this->instantiated[$interface]);
    }

    public function get(string $class): ServiceInterface
    {
        if (isset($this->instantiated[$class])) {
            return $this->instantiated[$class];
        }

        $object = new $class(...$this->services[$class]);

        if (!$object instanceof ServiceInterface) {
            throw new InvalidArgumentException('Could not register service: is no instance of Service');
        }

        $this->instantiated[$class] = $object;

        return $object;
    }
}