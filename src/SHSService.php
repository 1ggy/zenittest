<?php

namespace ZenItTest;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use ZenItTest\Form\Form;

/**
 * Основной класс - сам сервис
 * Class SHSService
 * @package ZenItTest
 */
class SHSService implements ServiceInterface
{
    private Client $client;

    public function __construct(string $endpointAPI, array $httpHeaders = [])
    {
        $this->client = new Client([
            'base_uri' => $endpointAPI,
            'headers' => $httpHeaders,
            'timeout' => 2.0,
        ]);
    }

    protected function sendRequest(string $method, string $uri, array $options = []): ResponseInterface
    {
        $request = new Request($method, $uri);
        try {
            return $this->client->send($request, $options);
        } catch (RequestException $e) {
            //если приходит респонс с ошибкой, возвращаем этот респонс как есть
            return $e->getResponse();
        }
    }

    public function get(): ResponseInterface
    {
        return $this->sendRequest('GET', '/get');
    }

    /**
     * post запрос с возможностью отправить данные в форме
     * @param Form $form
     * @return ResponseInterface
     */
    public function post(Form $form): ResponseInterface
    {
        $options = [];
        if ($form->getFields()) {
            $options = ['multipart' => $form->getFields()];
        }
        return $this->sendRequest('POST', '/post', $options);
    }

    public function put(): ResponseInterface
    {
        return $this->sendRequest('PUT', '/put');
    }

    public function delete(): ResponseInterface
    {
        return $this->sendRequest('DELETE', '/delete');
    }

    public function patch(): ResponseInterface
    {
        return $this->sendRequest('PATCH', '/patch');
    }
}