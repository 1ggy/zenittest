##ТЗ
https://docs.google.com/document/d/1YOfez7LBMd2857UitGsuBfgbyIBvtZdgbujKm9LceE8/edit#heading=h.457bg661b947

##Развертывание проекта
docker-compose run php-fpm composer install

##Тестовый запрос
http://localhost:8070
test.php - тестовый клиент для сервиса

##Тесты
docker-compose run php-fpm ./vendor/bin/phpunit tests --testdox