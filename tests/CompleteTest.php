<?php declare(strict_types=1);

namespace ZenItTest\tests;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use ZenItTest\Form\FormBuilder;
use ZenItTest\Locator;
use ZenItTest\ServiceInterface;
use ZenItTest\SHSService;
use ZenItTest\UserDTO;

final class CompleteTest extends TestCase
{
    private ServiceInterface $shsService;

    public function setUp(): void
    {
        //конфигурация для сервиса
        $endpointAPI = 'https://httpbin.org';
        $httpHeaders = [
            'Authorization' => 'Bearer <token>',
            'Content-type' => 'application/json'
        ];

        //добавляем сервис в локатор
        $locator = Locator::getInstance();
        $locator->addService(SHSService::class, new SHSService($endpointAPI, $httpHeaders));

        //получаем сервис
        $this->shsService = $locator->get(SHSService::class);
    }

    public function testGetRequest(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->shsService->get();
        self::assertInstanceOf(ResponseInterface::class, $response);
        self::assertEquals(200, $response->getStatusCode());
    }

    public function testPostRequest(): void
    {
        //объект с данными пользователя
        $user = new UserDTO(1, 'asdf@mail.ru');

        //Строитель форм
        $formBuilder = new FormBuilder();
        $form = $formBuilder->createForm()
            ->addTextField('id', $user->getId())
            ->addTextField('email', $user->getEmail())
            ->getForm();

        /** @var ResponseInterface $response */
        $response = $this->shsService->post($form);

        self::assertInstanceOf(ResponseInterface::class, $response);
        self::assertEquals(200, $response->getStatusCode());
    }

    public function testPutRequest(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->shsService->put();
        self::assertInstanceOf(ResponseInterface::class, $response);
        self::assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteRequest(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->shsService->delete();
        self::assertInstanceOf(ResponseInterface::class, $response);
        self::assertEquals(200, $response->getStatusCode());
    }

    public function testPatchRequest(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->shsService->patch();
        self::assertInstanceOf(ResponseInterface::class, $response);
        self::assertEquals(200, $response->getStatusCode());
    }
}
