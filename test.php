<?php

$loader = require __DIR__ . '/vendor/autoload.php';

use ZenItTest\Form\FormBuilder;
use ZenItTest\Locator;
use ZenItTest\SHSService;
use ZenItTest\UserDTO;

//чтобы сделать вывод читаемым в браузере
echo '<pre>';

//конфигурация для сервиса
$endpointAPI = 'https://httpbin.org';
$httpHeaders = [
    'Authorization' => 'Bearer <token>',
    'Content-type' => 'application/json'
];

//добавляем сервис в локатор
$locator = Locator::getInstance();
$locator->addService(SHSService::class, new SHSService($endpointAPI, $httpHeaders));

//использование сервиса
$SHSService = $locator->get(SHSService::class);

//объект с данными пользователя
$user = new UserDTO(1, 'asdf@mail.ru');

//Строитель форм
$formBuilder = new FormBuilder();
$form = $formBuilder->createForm()
    ->addTextField('id', $user->getId())
    ->addTextField('email', $user->getEmail())
    ->getForm();

$result = $SHSService->post($form);
//$result = $SHSService->get();
//$result = $SHSService->put();
//$result = $SHSService->delete();
//$result = $SHSService->patch();

var_dump($result);
